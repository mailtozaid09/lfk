import { DrawerContentScrollView } from '@react-navigation/drawer';
import React from 'react'
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, Pressable, } from 'react-native';
import { colors } from '../../global/colors';
import { fontSize, Poppins } from '../../global/fontFamily';
import { media } from '../../global/media';


const DrawerSidebarMenu = (props) => {
    return (
        <SafeAreaView style={{flex: 1, backgroundColor: '#F1F1F1'}} >
            
                <View style={styles.detailsContainer} >

        
                    <Image source={media.Logo_LFK} style={{height: 100, width: 240, resizeMode: 'contain' }} />

                </View>

                

            <DrawerContentScrollView {...props} contentContainerStyle={{flex: 1}} >

                                    
                <View style={{flex: 1, justifyContent: 'space-between', marginBottom: 20}} >
                    <View>
                        <TouchableOpacity
                            onPress={() => props.navigation.navigate("Home")}
                            style={styles.drawerItem}
                            >
                            <Image source={media.home_tab} style={{height: 26, width: 26, marginRight: 12}} />
                            <Text style={styles.drawerItemText}>Home</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => props.navigation.navigate("Home")}
                            style={styles.drawerItem}
                            >
                            <Image source={media.about} style={{height: 26, width: 26, marginRight: 12}} />
                            <Text style={styles.drawerItemText}>About</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => props.navigation.navigate("Home")}
                            style={styles.drawerItem}
                            >
                            <Image source={media.blog} style={{height: 26, width: 26, marginRight: 12}} />
                            <Text style={styles.drawerItemText}>Blog</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => props.navigation.navigate("Home")}
                            style={styles.drawerItem}
                            >
                            <Image source={media.faq} style={{height: 26, width: 26, marginRight: 12}} />
                            <Text style={styles.drawerItemText}>FAQ</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => props.navigation.navigate("Home")}
                            style={styles.drawerItem}
                            >
                            <Image source={media.contactus} style={{height: 26, width: 26, marginRight: 12}} />
                            <Text style={styles.drawerItemText}>Contact ss</Text>
                        </TouchableOpacity>

                    </View>


                    <TouchableOpacity
                        onPress={() => props.navigation.navigate("LoginStack")}
                        style={styles.drawerItem}
                    >
                        {/* <Image source={media.logout} style={{height: 26, width: 26, marginRight: 12}} /> */}
                        <Text style={styles.drawerItemText}>Log Out</Text>
                    </TouchableOpacity>
                </View>
            </DrawerContentScrollView>
        </SafeAreaView>
    )
}


const styles = StyleSheet.create({
    drawerItem: {
        padding: 10,
        paddingHorizontal: 20,
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 0,

    },
    drawerItemText: {
        fontSize: fontSize.SubTitle,
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    detailsContainer: {
        //alignItems: 'center',
        //borderBottomWidth: 1,
        padding: 20,
        paddingBottom: 20,
        borderColor: colors.white,
    },
    detailsTitle: {
        fontSize: fontSize.Title,
        fontFamily: Poppins.Medium,
        color: colors.white,
    },
    detailsSubTitle: {
        fontSize: fontSize.Body,
        fontFamily: Poppins.Medium,
        color: colors.white,
    },
})
export default DrawerSidebarMenu