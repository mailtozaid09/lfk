import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import HomeScreen from '../home';
import DrawerSidebarMenu from './DrawerSidebarMenu';
import { media } from '../../global/media';
import { Poppins, fontSize } from '../../global/fontFamily';
import { colors } from '../../global/colors';
import Tabbar from '../tabbar';
import { screenWidth } from '../../global/constants';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();


const DrawerStack = () => {

    function DetailsScreen({ navigation }) {
        return (
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Button
              onPress={() => navigation.navigate('Profile')}
              title="Go to notifications"
            />
          </View>
        );
      }
      function ProfileScreen({ navigation }) {
        return (
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Button
              onPress={() => navigation.navigate('Notifications')}
              title="Go to notifications == ProfileScreen"
            />
          </View>
        );
      }
      
      function NotificationsScreen({ navigation }) {
        return (
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Button onPress={() => navigation.goBack()} title="Go back home" />
          </View>
        );
      }

      return (
        <Drawer.Navigator 
            drawerContent={(props) => <DrawerSidebarMenu {...props} />}
            initialRouteName="Tabbar"
            screenOptions={({ navigation }) => ({
              headerLeft: props => <TouchableOpacity onPress={() => navigation.openDrawer()} >
                <Image  source={media.menu} style={{height: 30, width: 30, resizeMode: 'contain', marginLeft: 20, marginBottom: 10}} />
              </TouchableOpacity>,
            })}
        >
            <Drawer.Screen name="Tabbar" component={Tabbar} 
              options={{
                headerShown: true,
                headerStyle: {backgroundColor: colors.white, height: 100},
                headerTitleStyle: {
                  fontSize: fontSize.Title,
                  fontFamily: Poppins.Medium,
                  color: colors.white,
                  marginBottom: 20
                },
                headerTitle: () => (
                  <View style={{marginLeft: screenWidth*0.15, width: '100%'}} >
                    <Image  source={media.Logo_LFK} style={{height: 140, width: 100, resizeMode: 'contain',}} />
                  </View>
                ),
                headerRight: () => (
                  <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}} >
                    <TouchableOpacity onPress={() => {}} >
                    <Image  source={media.lang} style={{height: 26, width: 26, resizeMode: 'contain', }} />
                  </TouchableOpacity>
                  <Text style={{fontSize: 20, fontWeight: '400', color: colors.black, marginRight: 20, marginLeft: 10 }} >EN</Text>
                  </View>
                ),
            }}
            />
            <Drawer.Screen name="Notifications" component={NotificationsScreen} />
        </Drawer.Navigator>
    );
}

export default DrawerStack