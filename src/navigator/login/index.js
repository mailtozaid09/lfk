import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, ViewBase, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import LoginScreen from '../../screens/authentication/login';

import { colors } from '../../global/colors';
import { media } from '../../global/media';


const Stack = createStackNavigator();


const LoginStack = ({navgation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Login" 
        >
            <Stack.Screen
                name="Login"
                component={LoginScreen}
                options={({navigation}) => ({
                    headerShown: false,
                })}
            />
        </Stack.Navigator>
    );
}

const styles = StyleSheet.create({
    headerStyle: {
        backgroundColor: colors.black,
        elevation: 0,
        shadowOpacity: 0,
        borderBottomWidth: 0,
    }
})

export default LoginStack