import React, {useState, useEffect} from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';


import LoginStack from './login';
import Tabbar from './tabbar';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useSelector } from 'react-redux';
import DrawerStack from './drawer';

const Stack = createStackNavigator();


const MainNavigator = ({navgation}) => {

    return (
        <Stack.Navigator 
            initialRouteName="LoginStack"
        >
            <Stack.Screen
                name="LoginStack"
                component={LoginStack}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="DrawerStack"
                component={DrawerStack}
                options={{
                    headerShown: false
                }}
            />
        </Stack.Navigator>
    );
}

export default MainNavigator