import React,{useEffect, useState} from 'react'
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, Image, LogBox,  } from 'react-native'

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';

import AsyncStorage from '@react-native-async-storage/async-storage';

import HomeStack from '../home';

import { media } from '../../global/media';
import { colors } from '../../global/colors';
import SearchScreen from '../../screens/dashboard/search';
import CartScreen from '../../screens/dashboard/cart';
import ProfileScreen from '../../screens/dashboard/profile';


const Tab = createBottomTabNavigator();


export default function Tabbar({navigation}) {

    LogBox.ignoreAllLogs(true)
    
    const [userDetails, setUserDetails] = useState({});

    useEffect(() => {
        //getUserDetails()
    }, [])
    

    const getUserDetails = async () => {
        let user = await AsyncStorage.getItem('user_details');  
        let user_details = JSON.parse(user);  
        setUserDetails(user_details)

        console.log('====================================');
        console.log("Tabbar ", user_details);
        console.log('====================================');

        if(user_details ==null){
            navigation.navigate('LoginStack')
        }
    }

    return (
        <Tab.Navigator 
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                let iconName, routeName, height = 24, width = 24 ;

                if (route.name === 'HomeStack') {
                    iconName = !focused ? media.home : media.home_fill
                    routeName = 'Home' 
                } 
                else if (route.name === 'Search') {
                    iconName = !focused ?  media.search : media.search_fill
                    routeName = 'Search' 
                } 
                else if (route.name === 'Cart') {
                    iconName = !focused ?  media.cart : media.cart_fill
                    routeName = 'Cart' 
                } 
                else if (route.name === 'Profile') {
                    iconName = !focused ?  media.user : media.user_fill
                    routeName = 'Profile' 
                } 
                return(
                    <View style={{alignItems: 'center', justifyContent: 'center',}} >
                        <Image source={iconName} style={{height: height, width: width,  resizeMode: 'contain'}}/>
                         <Text style={{fontSize: 10, color: focused ? 'white' : '#9E9E9E', fontWeight: '600'}} >{routeName}</Text>
                    </View>
                );
                },
            })}
        >
            <Tab.Screen 
                name="HomeStack" 
                component={HomeStack} 
                options={({navigation, route}) => ({
                    headerShown: false,
                    tabBarShowLabel: false,
                    tabBarLabelStyle:{fontWeight:"bold", fontSize:12, marginBottom: 12, },
                        tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                     
                        if (routeName === 'WhoIsWatching') {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                    })}
                />
             <Tab.Screen 
                name="Search" 
                component={SearchScreen} 
                options={({navigation, route}) => ({
                    headerShown: false,
                    tabBarShowLabel: false,
                    tabBarLabelStyle:{fontWeight:"bold", fontSize:12, marginBottom: 12, },
                        tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                     
                        if (routeName === 'WhoIsWatching') {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                    })}
                />
            <Tab.Screen 
                name="Cart" 
                component={CartScreen} 
                options={({navigation, route}) => ({
                    headerShown: false,
                    tabBarShowLabel: false,
                    tabBarLabelStyle:{fontWeight:"bold", fontSize:12, marginBottom: 12, },
                        tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                     
                        if (routeName === 'WhoIsWatching') {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                    })}
                />
            <Tab.Screen 
                name="Profile" 
                component={ProfileScreen} 
                options={({navigation, route}) => ({
                    headerShown: false,
                    tabBarShowLabel: false,
                    tabBarLabelStyle:{fontWeight:"bold", fontSize:12, marginBottom: 12, },
                        tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                     
                        if (routeName === 'WhoIsWatching') {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                    })}
                />
        </Tab.Navigator>
    );
}



const styles = StyleSheet.create({
    tabbarStyle: {
        height: 70,
        paddingBottom: 5,
        alignItems:"center",
        justifyContent: 'center',
        backgroundColor: colors.primary,
    },
    headerStyle: {
        backgroundColor: colors.black,
        elevation: 0,
        shadowOpacity: 0,
        borderBottomWidth: 0,
    }
})
