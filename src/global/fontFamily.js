export const Poppins = {
    Bold: 'Poppins-Bold',
    Light: 'Poppins-Light',
    Medium: 'Poppins-Medium',
    SemiBold: 'Poppins-SemiBold',
    Regular: 'Poppins-Regular',
};
  

export const fontSize = {
  Heading: 30,
  SubHeading: 24,
  Title: 20,
  SubTitle: 16,
  Body: 14,
}