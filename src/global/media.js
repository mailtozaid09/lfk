export const media = {
    password: require('../assets/images/password.png'),
    email: require('../assets/images/email.png'),
    eye: require('../assets/images/eye.png'),
    menu: require('../assets/images/menu.png'),
    fb: require('../assets/images/fb.png'),

    lang: require('../assets/images/lang.png'),

    mens_clothes: require('../assets/images/mens-clothes.png'),
    woman_clothes: require('../assets/images/woman-clothes.png'),
    jewelry: require('../assets/images/jewelry.png'),
    electronics: require('../assets/images/electronics.png'),

    Logo_LFK: require('../assets/images/Logo_LFK.png'),
    
    home: require('../assets/images/tabicon/home.png'),
    home_fill: require('../assets/images/tabicon/home_fill.png'),
    search: require('../assets/images/tabicon/search.png'),
    search_fill: require('../assets/images/tabicon/search_fill.png'),

    cart: require('../assets/images/tabicon/cart.png'),
    cart_fill: require('../assets/images/tabicon/cart_fill.png'),
    user: require('../assets/images/tabicon/user.png'),
    user_fill: require('../assets/images/tabicon/user_fill.png'),

    about: require('../assets/images/drawer/about.png'),
    blog: require('../assets/images/drawer/blog.png'),
    contactus: require('../assets/images/drawer/contactus.png'),
    faq: require('../assets/images/drawer/faq.png'),
    home_tab: require('../assets/images/drawer/home_tab.png'),
    

    
    
    
    netflixIcon: require('../assets/images/icon.png'),

    netflixText: require('../assets/images/logo.png'),

    comingsoon: require('../assets/images/comingsoon.png'),
    everyonewatching: require('../assets/images/everyonewatching.png'),
    top10: require('../assets/images/top10.png'),

    banner: require('../assets/images/banner.png'),

    close_circle: require('../assets/images/close_circle.png'),
    //search: require('../assets/images/search.png'),
    searchIcon: require('../assets/images/searchIcon.png'),
    cast: require('../assets/images/cast.png'),


    remind: require('../assets/images/remind.png'),
    info: require('../assets/images/info.png'),

    play: require('../assets/images/play.png'),
    play_circle: require('../assets/images/play_circle.png'),
    
    like: require('../assets/images/like.png'),
    share: require('../assets/images/share.png'),
    plus: require('../assets/images/plus.png'),

    edit: require('../assets/images/edit.png'),

    down_arrow: require('../assets/images/down_arrow.png'),
    right_arrow: require('../assets/images/right_arrow.png'),



    onboarding1: require('../assets/images/onboarding1.png'),
    onboarding2: require('../assets/images/onboarding2.png'),
    onboarding3: require('../assets/images/onboarding3.png'),
    onboarding4: require('../assets/images/onboarding4.png'),
    
    


    delete: require('../assets/images/delete.png'),
    account: require('../assets/images/account.png'),
    settings: require('../assets/images/settings.png'),
    mylist: require('../assets/images/mylist.png'),

    gamehandle: require('../assets/images/gamehandle.png'),
    language: require('../assets/images/language.png'),
    maturity: require('../assets/images/maturity.png'),
    subtitle: require('../assets/images/subtitle.png'),
    autoplay: require('../assets/images/autoplay.png'),
    autopreview: require('../assets/images/autopreview.png'),

    kids: require('../assets/images/profiles/kids.png'),
    
    profile1: require('../assets/images/profiles/profile1.png'),
    profile2: require('../assets/images/profiles/profile2.png'),
    profile3: require('../assets/images/profiles/profile3.png'),

}
