export const colors = {
    accent: '#211f1f',

    dark: '#121212',
    red: '#e50914',
    light: '#fff',
    grey: '#8c8c8c',

    primary: '#EC1C24',
    cream: '#FAC0A4',
    darkGray: '#515151',
    yellow: '#F3EC19',

    primary2: '#D22F26',
    primary3: '#DF7375',

    black: '#000',

    light_red: '#FEA7A9',

    dark_blue: '#212c46',
    
   
    white: '#fdfdfd',
    
    dark_gray: '#737373',
    dark_gray2: '#323232',
    dark_gray3: '#191919',
    
    light_gray_1: '#E3E3E3',
    light_gray_2: '#D9D9D9',

    light_gray1: '#e6e6e6',
    light_gray2: '#cbcbcb',
    light_gray3: '#b2b2b2',
}