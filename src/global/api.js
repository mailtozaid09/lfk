const baseUrl = 'https://fakestoreapi.com'


export function getAllProducts() {
    return(
        fetch(
            `${baseUrl}/products?sort=desc`,
            {
              method: "GET",
            }
        )
        .then(res => res.json())
    );
}


export function getTodaysProduct() {
    return(
        fetch(
            `${baseUrl}/products?limit=4`,
            {
              method: "GET",
            }
        )
        .then(res => res.json())
    );
}


export function getAllCategories() {
    return(
        fetch(
            `${baseUrl}/products/categories`,
            {
              method: "GET",
            }
        )
        .then(res => res.json())
    );
}

