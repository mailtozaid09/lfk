import React, {useState, useEffect, useRef} from 'react'
import { Text, View, SafeAreaView, TouchableOpacity, Image, StyleSheet } from 'react-native'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'



const ProductHeading = ({title, onPress}) => {
    return (
        <View style={{marginBottom: 20, marginTop: 4, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} >
            <Text style={styles.title} >{title}</Text>
            <TouchableOpacity>
                <Text style={styles.viewAll}>View All</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    title: {
        fontSize: 20,
        color: colors.black,
        fontWeight: 'bold',
        fontFamily: Poppins.Bold,
    },
    viewAll: {
        fontSize: 16,
        color: '#FAC0A4',
        textDecorationLine: 'underline',
        fontFamily: Poppins.SemiBold,
    }
})

export default ProductHeading