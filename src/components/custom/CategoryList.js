import React, {useState, useEffect, useRef} from 'react'
import { Text, View, SafeAreaView, TouchableOpacity, Image, StyleSheet, FlatList, ScrollView } from 'react-native'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { screenWidth } from '../../global/constants'
import ProductHeading from './ProductHeading'
import { media } from '../../global/media'



const CategoryList = ({data, title}) => {

   
    return (

        <>
            <ProductHeading title={title} onPress={() => {}} />

            <ScrollView horizontal>
                {data?.map((item, index) => {

                    var str = item;
                    var titleString = str.charAt(0).toUpperCase() + str.slice(1);


                    return(
                        <TouchableOpacity  key={index} style={styles.productContainer} >  
                            <Image source={
                                item == "electronics"
                                ? media.electronics
                                : item == "jewelery"
                                ? media.jewelry
                                : item == "men's clothing" 
                                ? media.mens_clothes
                                : item == "women's clothing"
                                ? media.woman_clothes
                                : null
                            } style={{height: 60, width: 60, resizeMode: 'contain'}} />
                            <Text style={styles.title} >{titleString}</Text>
                        </TouchableOpacity>
                    )
                })}
            </ScrollView>
        </>
    )
}


const styles = StyleSheet.create({
    title: {
        fontSize: 15,
        marginTop: 6,
        color: colors.black,
        fontFamily: Poppins.Light,
    },
    productContainer: {
        width: 120,
        height: 120,
        margin: 6,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderRadius: 10,
        backgroundColor: colors.white
    },
})

export default CategoryList