import React, {useState, useEffect, useRef} from 'react'
import { Text, View, SafeAreaView, TouchableOpacity, Image, StyleSheet } from 'react-native'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { screenWidth } from '../../global/constants'
import ProductHeading from './ProductHeading'



const ProductList = ({data, title}) => {
    return (

        <>
            <ProductHeading title={title} onPress={() => {}} />
        
            <View style={{alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', flexWrap: 'wrap'}} >
                {data?.map((item, index) => (
                    <TouchableOpacity key={index} style={styles.productContainer} >
                        <View style={styles.imageContainer} >
                            <Image source={{uri: item.image}} style={styles.imageStyle} />
                        </View>

                        <View style={{marginTop: 4, height: 85 }} >
                            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} >
                                <Text style={styles.price}>Rs. {item.price}</Text>
                                <Text style={styles.rating}>{item.rating?.rate}k <Text style={{color: colors.black}} >Sold</Text></Text>
                            </View>
                            <Text numberOfLines={2} style={styles.title}>{item.title}</Text>
                        </View>
                    </TouchableOpacity>
                ))}
            </View>
        </>
    )
}


const styles = StyleSheet.create({
    title: {
        fontSize: 20,
        color: colors.black,
        fontFamily: Poppins.Light,
    },
    price: {
        fontSize: 16,
        color: colors.black,
        fontFamily: Poppins.SemiBold,
    },
    rating: {
        fontSize: 14,
        color: colors.primary,
        fontFamily: Poppins.SemiBold,
    },
    productContainer: {
        width: screenWidth/2-40,
        height: 220,
        margin: 6,
        marginBottom: 20,
    },
    imageStyle: {   
        height: 120, 
        width: '100%',
        resizeMode: 'contain',
    },
    imageContainer: {
        borderWidth: 1,
        padding: 10,
        width: '100%', 
        borderRadius: 10,
    }
})

export default ProductList