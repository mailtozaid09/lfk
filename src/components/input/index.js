import React from 'react'
import { Text, View, TouchableOpacity, Image, StyleSheet, TextInput } from 'react-native'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { media } from '../../global/media'




const Input = ({title, placeholder, icon, value, onShowPass, showPass, onChangeText, isPassword, inputColor}) => {
    return (
        <View>
            {/* <Text style={[styles.title, {color: colors.black}]} >{title}</Text> */}
            <View style={[styles.container, inputColor ? {borderWidth: 1, } : {backgroundColor: colors.light_gray_1}]} >
                <View style={{height: 60, width: 60, backgroundColor: colors.light_gray_2, borderRadius: 10, alignItems: 'center', justifyContent: 'center', marginRight: 12, }} >
                    <Image source={icon} style={{height: 24, width: 24, resizeMode: 'contain'}} />
                </View>
                
                <TextInput
                    autoCapitalize="none"
                    autoCorrect={false}
                    style={[styles.inputStyle, {color: colors.black} ]}
                    placeholder={placeholder}
                    onChangeText={onChangeText}
                    placeholderTextColor={colors.darkGray}
                    secureTextEntry={isPassword && !showPass ? true : false}
                />
                 {isPassword &&
                    <TouchableOpacity
                        //onPress={onShowPass}
                    >
                        <Image source={media.eye} style={{height: 24, width: 24}} />
                    </TouchableOpacity>
                }
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        borderRadius: 10,
        paddingHorizontal: 20,
        paddingLeft: 0,
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    inputStyle: {
        flex: 1,
        height: 60,
        marginRight: 20,
        fontSize: 16,
        fontFamily: Poppins.Medium
    },
    title: {
        fontSize: 16,
        color: colors.black,
        marginBottom: 8,
        fontFamily: Poppins.Bold
    }
})

export default Input