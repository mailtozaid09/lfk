import React from 'react'
import { Text, View, TouchableOpacity, Image, StyleSheet, ActivityIndicator } from 'react-native'
import { colors } from '../../global/colors'
import { media } from '../../global/media'
import { screenWidth } from '../../global/constants'
import { Poppins } from '../../global/fontFamily'


const FacebookButton = ({title, onPress, icon, loading, buttonColor}) => {

    return (
        <View style={{width: '100%', alignItems: 'center'}} >
            <TouchableOpacity
            onPress={onPress}
            style={[styles.container, {backgroundColor: buttonColor ? buttonColor : '#2374F2',}]}
        >
            {loading == true
            ? 
                <ActivityIndicator
                    size="large" 
                    color="#fff"
                />
            :
            <>
                {icon && <Image source={icon} style={{height: 24, width: 24, marginRight: 10}} />}
                <Text style={{fontSize: 20, fontFamily: Poppins.Bold, color: 'white'}} >{title}</Text>
            </>
            }
            
        </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 50,
        width: '100%',
        marginVertical: 10,
        marginTop: 20,
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        
    }
})

export default FacebookButton