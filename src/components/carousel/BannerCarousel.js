import React, {useState, useRef} from 'react';
import {Text, View, Dimensions, Image, StyleSheet, } from 'react-native';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import { screenWidth } from '../../global/constants';
import { colors } from '../../global/colors';
import { media } from '../../global/media';


export const SLIDER_WIDTH = screenWidth
export const ITEM_WIDTH = Math.round(SLIDER_WIDTH - 40);

const data = [
    {
        id: 1,
        image: media.banner,
    },
    {
        id: 2,
        image: media.banner,
    },
    {
        id: 3,
        image: media.banner,
    },
    {
        id: 4,
        image: media.banner,
    },
    {
        id: 5,
        image: media.banner,
    },
];

const renderItem = ({item, index}) => {
    return (
        <View
            key={index}
            style={styles.carouselContainer}>
            <Image source={item.image} style={{width: '100%', height: 150, resizeMode: 'stretch', borderRadius: 20}} />
        </View>
    );
};

const BannerCarousel = () => {
    const [index, setIndex] = useState(0);
    const isCarousel = useRef(null);
    return (
        <View style={{marginVertical: 10, marginBottom: 0, alignItems: 'center'}}>
            <Carousel
                loop={true}
                autoplay={true}
                ref={isCarousel}
                data={data}
                renderItem={renderItem}
                sliderWidth={SLIDER_WIDTH}
                itemWidth={ITEM_WIDTH}
                onSnapToItem={index => setIndex(index)}
            />
            <Pagination

                dotsLength={data.length}
                activeDotIndex={index}
                carouselRef={isCarousel}
                dotStyle={{
                width: 10,
                height: 10,
                borderRadius: 5,
                marginHorizontal: 8,
                backgroundColor: colors.dark_blue,
                }}
                tappableDots={true}
                inactiveDotStyle={{
                backgroundColor: 'black',
                // Define styles for inactive dots here
                }}
                inactiveDotOpacity={0.4}
                inactiveDotScale={0.6}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    carouselContainer: {
        borderWidth: 1,
        borderRadius: 10,
        height: 152,
        alignItems: 'center',
        backgroundColor: 'white',
        borderColor: colors.dark_blue,
    }
})


export default BannerCarousel