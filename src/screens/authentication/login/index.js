import React, {useState, useEffect, useRef} from 'react'
import { SafeAreaView, Text, View, TouchableOpacity, Image, StyleSheet } from 'react-native'
import { colors } from '../../../global/colors'
import { Poppins } from '../../../global/fontFamily'
import Input from '../../../components/input'
import { media } from '../../../global/media'
import LoginButton from '../../../components/button/LoginButton'
import FacebookButton from '../../../components/button/FacebookButton'

const LoginScreen = ({navigation}) => {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
  
    const [showPass, setShowPass] = useState(false);

    return (
        <SafeAreaView style={styles.container} >
            <View style={{paddingHorizontal: 30, flex: 1, justifyContent: 'space-between', width: '100%'}} >
                <Text></Text>

                <View>
                    <Text style={styles.headingTitle} >Welcome To</Text>
                    <Text style={styles.headingSubTitle} >LFK</Text>

                    <Input
                        title="Email"
                        placeholder="Email Address"
                        icon={media.email}
                        onChangeText={text => setEmail(text)}
                    />

                    <Input
                        title="Password"
                        placeholder="Password"
                        icon={media.password}
                        isPassword={true}
                        showPass={showPass}
                        onChangeText={text => setPassword(text)}
                        onShowPass={() => setShowPass(!showPass)}
                    />

                    <View style={{alignItems: 'flex-end'}} >
                        <Text style={styles.forgot} >Forgot Password?</Text>
                    </View>

                    <FacebookButton
                        title="LOGIN WITH FACEBOOK"
                        icon={media.fb}
                    />
                </View>

                <View>
                    <LoginButton
                        title="SIGN IN"
                        onPress={() => {navigation.navigate('DrawerStack')}}
                    />

                    <View style={{alignItems: 'center', marginBottom: 20}} >
                        <Text style={styles.didnot} >Don't have an account? <Text style={{color: colors.primary, fontFamily: Poppins.SemiBold}} >Create one</Text></Text>
                    </View>
                </View>


            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        backgroundColor: colors.white,
    },
    forgot: {
        fontSize: 16,
        fontFamily: Poppins.Medium,
        color: colors.black
    },
    didnot: {
        fontSize: 16,
        fontFamily: Poppins.Medium,
        color: colors.black
    },
    headingTitle: {
        fontSize: 24,
        fontFamily: Poppins.Medium,
        color: colors.black
    },
    headingSubTitle: {
        fontSize: 34,
        fontFamily: Poppins.Bold,
        color: colors.primary,
        marginBottom: 20,
    },
})

export default LoginScreen