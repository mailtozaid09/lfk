import React, {useState, useEffect, useRef} from 'react'
import { Text, View, SafeAreaView, TouchableOpacity, Image, StyleSheet } from 'react-native'
import { colors } from '../../../global/colors'

const SearchScreen = ({navigation}) => {
    return (
        <SafeAreaView style={styles.container} >
            <View>
                <Text>SearchScreen</Text>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
    }
})

export default SearchScreen