import React, {useState, useEffect, useRef} from 'react'
import { Text, View, SafeAreaView, TouchableOpacity, Image, StyleSheet, ScrollView } from 'react-native'
import { colors } from '../../../global/colors'


import SearchInput from '../../../components/input/SearchInput'
import BannerCarousel from '../../../components/carousel/BannerCarousel'

import { getAllCategories, getAllProducts, getTodaysProduct } from '../../../global/api'

import ProductList from '../../../components/custom/ProductList'

import { screenWidth } from '../../../global/constants'
import CategoryList from '../../../components/custom/CategoryList'

const HomeScreen = ({navigation}) => {

    const [productData, setProductData] = useState([]);
    const [todaysData, setTodaysData] = useState([]);
    const [categoriesData, setCategoriesData] = useState([]);

    useEffect(() => {
        getAllProductsFunc()
        getAllCategoriesFunc()
        getTodaysProductFunc()
    }, [])
    
    const getAllProductsFunc = () => {
        getAllProducts()
        .then((resp) => {
            setProductData(resp)
            //console.log("resp > ", resp);
        })
        .then((err) => {
            console.log("err > ", err);
        })
    }

      
    const getTodaysProductFunc = () => {
        getTodaysProduct()
        .then((resp) => {
            setTodaysData(resp)
            //console.log("resp > ", resp);
        })
        .then((err) => {
            console.log("err > ", err);
        })
    }

    const getAllCategoriesFunc = () => {
        getAllCategories()
        .then((resp) => {
            setCategoriesData(resp)
            //console.log("resp > ", resp);
        })
        .then((err) => {
            console.log("err > ", err);
        })
    }

    
    

    return (
        <SafeAreaView style={styles.container} >

            <View style={{paddingHorizontal: 20}} >
                <SearchInput 
                        placeholder='Search'
                    />  
            </View>
            <ScrollView contentContainerStyle={{}} >
                
                <View style={{padding: 20}} >  
                    

                    <BannerCarousel />

                    <CategoryList title="Categories" data={categoriesData} onPress={() => {}} />

                    <ProductList title="Today's Deal" data={todaysData} onPress={() => {}} />
                    
                    <ProductList title="Featured Products" data={productData} onPress={() => {}} />

                </View>
                
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        backgroundColor: colors.white,
    }
})

export default HomeScreen