import React, { useState, useEffect } from 'react';
import { Text, View, LogBox, StatusBar, SafeAreaView, Image } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import Navigator from './src/navigator';

import { Provider, useSelector, } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import { store, persistor } from './src/store';
import SplashScreen from 'react-native-splash-screen';

LogBox.ignoreAllLogs(true);


const App = () => {

    useEffect(() => {
        setTimeout(() => {
            SplashScreen.hide(); 
        }, 2000);
      }, []);

    return (
        <Provider store={store}>
            <PersistGate persistor={persistor}>
                <NavigationContainer>
                    <StatusBar backgroundColor = "#0D0D0D" hidden={true}  />  
                    
                    <Navigator />
                </NavigationContainer>
            </PersistGate>
        </Provider>
    )
}

export default App
